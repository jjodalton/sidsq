//************************************
// Lead Voice Two Engine & Arpeggiator
//************************************
void leadTwoArpeggiator(byte dataType, byte noteNumber,
                        byte noteVelocity, byte noteNumberFromArray,
                        unsigned long noteDurationFromArray) {

  static byte arpNoteNumberArray[8];
  static unsigned long arpNoteDurationArray[8];
  static boolean arpeggiatorOn = LOW;
  unsigned long currentMillis;
  static unsigned long previousMillis;
  unsigned long elapsedTimeInMillis;
  static unsigned int arpeggiatorInterval = 20;
  static byte currentArpArrayPosition;
  static boolean arpeggiatorRunning = LOW;
  byte transitionNote;

  switch (dataType) {
    case 1: // Main Loop Run
      if ((arpeggiatorOn == HIGH) && (arpeggiatorRunning == HIGH)) {
        currentMillis = millis();
        elapsedTimeInMillis = currentMillis - previousMillis;
        if (elapsedTimeInMillis >= arpeggiatorInterval) {
          mainLeadTwoEngine(3, arpNoteNumberArray[currentArpArrayPosition],
                            0, HIGH, 1, 0);
          if (currentArpArrayPosition < 7) {
            currentArpArrayPosition++;
          }
          else {
            currentArpArrayPosition = 0;
          }
          while (arpNoteNumberArray[currentArpArrayPosition] == 0) {
            if (currentArpArrayPosition < 7) {
              currentArpArrayPosition++;
            }
            else {
              currentArpArrayPosition = 0;
            }
          }
          mainLeadTwoEngine(2, arpNoteNumberArray[currentArpArrayPosition],
                            0, HIGH, 1, 0);
          previousMillis = millis();
        }
      }
      break;

    case 2: // Note On
      if (arpeggiatorOn == HIGH) {
        placeNoteInFirstEmptyArrayPosition(arpNoteNumberArray, arpNoteDurationArray, noteNumber);
        arpeggiatorRunning = testArrayContentsForAtLeastTwoNotes(arpNoteNumberArray) ? HIGH : LOW;
        if (arpeggiatorRunning == LOW) {
          mainLeadTwoEngine(2, noteNumber, 0, HIGH, 0, 0);
        }
      }
      break;

    case 3: // Note Off
      if (arpeggiatorOn == HIGH) {
        eraseThisNoteInArray(arpNoteNumberArray, arpNoteDurationArray, noteNumber);
        arpeggiatorRunning = testArrayContentsForAtLeastTwoNotes(arpNoteNumberArray) ? HIGH : LOW;
        if (arpeggiatorRunning == LOW) {
          mainLeadTwoEngine(3, noteNumber, 0, HIGH, 0, 0);
          if (testArrayContentsForNoNotes(arpNoteNumberArray) == LOW) {
            mainLeadTwoEngine(2, findSingleNoteInArray(arpNoteNumberArray), 0, HIGH, 0, 0);
          }
        }
      }
      break;

    case 4: // CC4: Arpeggiator On/Off
      if ((noteNumber > 0) && (arpeggiatorOn == LOW)) {
        arpeggiatorOn = HIGH;
        mainLeadTwoEngine(8, 0, 0, HIGH, 0, 0);
        arpeggiatorRunning = testArrayContentsForAtLeastTwoNotes(arpNoteNumberArray) ? HIGH : LOW;
      }
      if (noteNumber == 0) {
        arpeggiatorOn = LOW;
        mainLeadTwoEngine(3, arpNoteNumberArray[currentArpArrayPosition], 0, HIGH, 0, 0);
        transitionNote = mostRecentNoteInArray(arpNoteNumberArray, arpNoteDurationArray);
        if (transitionNote > 0) {
          mainLeadTwoEngine(2, transitionNote, 0, HIGH, 0, 0);
        }
        for (int i = 0; i < 8; i++) {
          mainLeadTwoEngine(8, i, 0, LOW, arpNoteNumberArray[i], arpNoteDurationArray[i]);
        }
      }
      break;

    case 5: // Changes Arpeggiator Speed
      arpeggiatorInterval = map(noteNumber, 0, 127, 20, 200);
      break;

    case 6: // Accepts noteNumberArray and noteDurationArray
      arpNoteNumberArray[noteNumber] = noteNumberFromArray;
      arpNoteDurationArray[noteNumber] = noteDurationFromArray;
      break;
  }
}


void mainLeadTwoEngine(byte dataType, byte noteNumber, float floatData,
                       boolean noteComingFromArpeggiator, byte arpNoteNumberFromArray,
                       unsigned long arpNoteDurationFromArray) {

  static boolean insideRun = LOW;
  static byte noteNumberArray[8];
  static unsigned long noteDurationArray[8];
  static boolean portamentoOn = HIGH;
  static boolean portamentoRunning = LOW;
  static boolean portamentoDirection;
  unsigned long currentMicros;
  static unsigned long previousPortamentoChange;
  static unsigned long portamentoSpeedInMicros = 50;
  float portamentoStepInHertz;
  static float insidePitchInHertz;
  static float insideTargetPitchInHertz;
  static float insidePitchInHertzAfterFactors;
  static float insideBendFactor = 1.0;
  static boolean arpeggiatorOn = LOW;
  // static unsigned long decayInterval;
  // static boolean decayOn = LOW;
  // static byte currentDecayLevel = 127;
  // static unsigned long previousDecayMicros;
  static const byte leadTwoChannel = 2;
  static const byte leadTwoChip = 0;

  static float insideVibFactor = 1.0;
  static byte insideCC1 = 0;
  static float vibRateDivisor = 23.0;
  unsigned long currentVibCountMicros;
  static unsigned long sineVibSpeedStepsInMicros = 2000;
  static int vibAmount = 0;
  static float vibAmount2 = 0.0;
  static float vibFactor = 1.0;
  static int modWheelFactor = 0;
  static unsigned long previousVibCountMicros = 0;
  static unsigned long currentMillis;

  static byte attackAmount = 0;
  static byte decayAmount = 0;
  static byte sustainAmount = 15;
  static byte releaseAmount = 0;
  static unsigned int adsr = 0xF0;

  static byte basicWaveform = B1000000;
  byte basicWaveformNumber = 0;
  static byte waveformModifier = B0;
  byte waveformModifierNumber = 0;
  static byte waveformCombo = B1000000;

  static unsigned int pwmAmount = 2048;

  switch (dataType) {
    case 1: // Main Loop Run
      if ((insideRun == HIGH) && (portamentoOn == HIGH) &&
          (portamentoRunning == HIGH)) {

        currentMicros = micros();
        if ((portamentoDirection == LOW) && (currentMicros - previousPortamentoChange >= portamentoSpeedInMicros)) {

          portamentoStepInHertz = 2.0 * (insidePitchInHertz / 1000.0);
          insidePitchInHertz = insidePitchInHertz - portamentoStepInHertz;
          previousPortamentoChange = micros();

          if (insidePitchInHertz <= insideTargetPitchInHertz) {
            portamentoRunning = LOW;
            insidePitchInHertz = insideTargetPitchInHertz;
          }
          insidePitchInHertzAfterFactors = insidePitchInHertz * insideBendFactor * insideVibFactor;
          writeFrequency(insidePitchInHertzAfterFactors, leadTwoChannel, leadTwoChip);
        }
        if ((portamentoDirection == HIGH) && (currentMicros - previousPortamentoChange >= portamentoSpeedInMicros)) {

          portamentoStepInHertz = 2.0 * (insidePitchInHertz / 1000.0);
          insidePitchInHertz = insidePitchInHertz + portamentoStepInHertz;
          previousPortamentoChange = micros();
          if (insidePitchInHertz >= insideTargetPitchInHertz) {
            portamentoRunning = LOW;
            insidePitchInHertz = insideTargetPitchInHertz;
          }
          insidePitchInHertzAfterFactors = insidePitchInHertz * insideBendFactor * insideVibFactor;
          writeFrequency(insidePitchInHertzAfterFactors, leadTwoChannel, leadTwoChip);
        }
      }
      if (insideCC1 > 0) {
        currentVibCountMicros = micros();

        // Limits how often the chip can calculate the sine function.
        if  ((currentVibCountMicros - previousVibCountMicros) >= sineVibSpeedStepsInMicros) {
          currentMillis = millis();

          //range of  1 to -1, sclaed to time, factored to 100 to -100
          vibAmount = sin(((float)currentMillis * (float)vibRateFactor) / (float)vibRateDivisor) * 100.0;
          previousVibCountMicros = micros();

          //Scales vibAmount to very small positive or negative amounts
          vibAmount2 = (float)vibAmount * vibDepth;
          modWheelFactor = map(insideCC1, 0, 127, 0, 2000);
          insideVibFactor = 1.0 + (vibAmount2 * ((float)modWheelFactor / 1000));

          insidePitchInHertzAfterFactors = insidePitchInHertz * insideBendFactor * insideVibFactor;
          writeFrequency(insidePitchInHertzAfterFactors, leadTwoChannel, leadTwoChip);
        }
      }
      else {
        insideVibFactor = 1.0;
      }
      break;

    case 2: // Note On
      if ((arpeggiatorOn == LOW) || (noteComingFromArpeggiator == HIGH)) {
        if (insideRun == LOW) {
          insidePitchInHertz = 440.00 *
                               pow(2, ((noteNumber - 69) / 12.00));
          insidePitchInHertzAfterFactors = insidePitchInHertz * insideBendFactor * insideVibFactor;
          writeFrequency(insidePitchInHertzAfterFactors, leadTwoChannel, leadTwoChip);
          if (arpeggiatorOn == LOW) {
            placeNoteInFirstEmptyArrayPosition(noteNumberArray, noteDurationArray, noteNumber);
          }
          insideRun = HIGH;
          writeWaveform((waveformCombo | 0x1), leadTwoChannel, leadTwoChip);
          portamentoRunning = LOW;
        }
        else if ((insideRun == HIGH) && (arpeggiatorOn == LOW)) {
          insideTargetPitchInHertz = 440.00 * pow(2, ((noteNumber - 69) / 12.00));
          portamentoRunning = HIGH;
          placeNoteInFirstEmptyArrayPosition(noteNumberArray, noteDurationArray, noteNumber);
          if (insideTargetPitchInHertz > insidePitchInHertz) {
            portamentoDirection = HIGH;
          }
          else {
            portamentoDirection = LOW;
          }
        }
      }
      break;

    case 3: // Note Off
      if ((arpeggiatorOn == LOW) || (noteComingFromArpeggiator == HIGH)) {
        if (arpeggiatorOn == LOW) {
          eraseThisNoteInArray(noteNumberArray, noteDurationArray, noteNumber);
          if (mostRecentNoteInArray(noteNumberArray, noteDurationArray) == 0) {

            insideRun = LOW;
            writeWaveform((waveformCombo & B11111110), leadTwoChannel, leadTwoChip);
          }
          else {
            mainLeadTwoEngine(2, mostRecentNoteInArray(noteNumberArray, noteDurationArray), 0, LOW, 0, 0);
          }
        }
        if ((arpeggiatorOn == HIGH) && (arpNoteNumberFromArray == 0)) {
          insideRun = LOW;
          writeWaveform((waveformCombo & B11111110), leadTwoChannel, leadTwoChip);
        }

        // If notes are coming from main arp sequencer.
        if ((arpeggiatorOn == HIGH) && (arpNoteNumberFromArray == 1)) {
          insideRun = LOW;
          writeWaveform((waveformCombo & B11111110), leadTwoChannel, leadTwoChip);
        }
      }
      break;

    case 4: // Pitch Bend
      insideBendFactor = floatData;
      insidePitchInHertzAfterFactors = insidePitchInHertz * insideBendFactor * insideVibFactor;
      writeFrequency(insidePitchInHertzAfterFactors, leadTwoChannel, leadTwoChip);
      break;

    case 5: // ModWheel
      insideCC1 = noteNumber;
      break;

    case 6: //
      break;

    case 7: // CC5 Arp Speed Change
      break;

    case 8: // Arpeggiator Has Turned On Or Off.  Receive or transmit array.
      if (noteComingFromArpeggiator == HIGH) {
        arpeggiatorOn = HIGH;
        for (byte i = 0; i < 8; i++) {
          leadTwoArpeggiator(6, i, 0, noteNumberArray[i], noteDurationArray[i]);
        }
      }
      else {
        arpeggiatorOn = LOW;
        noteNumberArray[noteNumber] = arpNoteNumberFromArray;
        noteDurationArray[noteNumber] = arpNoteDurationFromArray;
      }
      break;

    case 9: // Basic Waveform Selection
      basicWaveformNumber = map(noteNumber, 0, 127, 0, 7);
      switch (basicWaveformNumber) {
        case 0: // Pulse Only
          basicWaveform = B1000000;
          break;

        case 1: // Saw Only
          basicWaveform = B100000;
          break;

        case 2: // Triangle Only
          basicWaveform = B10000;
          break;

        case 3: // Pulse/Saw
          basicWaveform = B1100000;
          break;

        case 4: // Pulse/Triangle
          basicWaveform = B1010000;
          break;

        case 5: // Saw/Triangle
          basicWaveform = B110000;
          break;

        case 6: // Pulse/Saw/Triangle
          basicWaveform = B1110000;
          break;

        case 7: // Noise
          basicWaveform = B10000000;
          break;
      }
      waveformCombo = basicWaveform | waveformModifier;
      if (insideRun == HIGH) {
        writeWaveform((waveformCombo | 0x1), leadTwoChannel, leadTwoChip);
      }
      else {
        writeWaveform((waveformCombo & B11111110), leadTwoChannel, leadTwoChip);
      }
      break;

    case 10: // Waveform Modifier Selection
      waveformModifierNumber = map(noteNumber, 0, 127, 0, 3);
      switch (waveformModifierNumber) {
        case 0: // Normal
          waveformModifier = B0;
          break;

        case 1: // Ring Mod
          waveformModifier = B100;
          break;

        case 2: // Sync
          waveformModifier = B10;
          break;

        case 3: // Ring Mod and Sync
          waveformModifier = B110;
          break;
      }
      waveformCombo = basicWaveform | waveformModifier;
      if (insideRun == HIGH) {
        writeWaveform((waveformCombo | 0x1), leadTwoChannel, leadTwoChip);
      }
      else {
        writeWaveform((waveformCombo & B11111110), leadTwoChannel, leadTwoChip);
      }
      break;

    case 11: // Attack Amount
      attackAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadTwoChannel, leadTwoChip);
      break;

    case 12: // Decay Amount
      decayAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadTwoChannel, leadTwoChip);
      break;

    case 13: // Sustain Amount
      sustainAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadTwoChannel, leadTwoChip);
      break;

    case 14: // Release Amount
      releaseAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadTwoChannel, leadTwoChip);
      break;

    case 15: // PWM Amount
      pwmAmount = map(noteNumber, 0, 127, 2048, 0);
      writePWM(pwmAmount, leadTwoChannel, leadTwoChip);
      break;

    case 16: // Portamento Speed
      portamentoSpeedInMicros = map(noteNumber, 0, 127, 1, 2000);
      break;
  }
}
