//**************
// Intro Tunes
//**************
void Original_Intro() {
  writeFilter(0, 0, 0);
  for (byte i = 1; i < 4; i++) {
    writePWM(2048, i, 0);
    writeADSR(0xF0, i, 0);
    for (byte j = 1; j < 4; j++) {
      writeFrequency(100 * j, i, 0);
      writeWaveform(B01000001, i, 0);
      digitalWrite(LED, HIGH);
      delay(40);
      digitalWrite(LED, LOW);
      delay(40);
    }
  }
  mainSampleEngine(2, 57, 127, 0);
  for (byte i = 1; i < 4; i++) {
    writeWaveform(B01000000, i, 0);
    writeFrequency(0, i, 0);
  }
  while (millis() < 3000) {
    mainSampleEngine(1, 0, 0, 0);
  }
  mainSampleEngine(3, 57, 127, 0);
}

void CloseEncounter_Mono() {

}

void CloseEncounter_Poly() {

}

