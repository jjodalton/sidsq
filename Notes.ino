// SID SYNTH 1.01
// -Brian Peters
// www.brianpeters.net
//
// - This is the single file version of the program for ease of copy/pasting.
// - Please see the schematic, feature list, channel/CC guide, and video
//    for more information.
//
//  TYRVQ
//
//
//  - MATERIALS
//
//    - 1x Teensy 2.0 Board
//      (www.pjrc.com)
//    - 1x MOS 8580 SID 28 PIN DIP
//      (eBay)
//    - 1x 1.0 MHz Full Can Oscillator
//      (eBay)
//    - 1x 9V Battery (or 9V Adaptor)
//    - 1x 9V Battery Connector (or 9V Adaptor jack)
//    - 2x 2200pF Polystyrene Capacitors
//    - 1x 1k Resistor
//    - 1x 2.2k Resistor
//    - 1x 330k Resistor
//    - 3x 10uF Electrolytic Capacitors
//    - 1x 1/4" Audio Jack (or 3.5mm)
//    - 1x 60+ Row Breadboard
//    - 1x USB A to Mini B Cable
//    - Lots of Wire
//
//
//  - UPLOADING PROGRAM TO TEENSY (Ideally before wiring everything.)
//  - After installing the teensy loader, go to
//      Tools > Board > Teensy 2.0
//        and
//      Tools > USB Type > MIDI
//    - Copy/Paste this program into the Arduino IDE
//    - Upload
//
//
//
// PROGRAM STRUCTURE:
//
//    Global Variables
//    PCM Sample Data
//    PCM Lengths
//
// setup
// loop
//
//
// AUXILIARY FUNCTIONS:
//
//    mostRecentNoteInArray
//    eraseOldestNoteInArray
//    eraseOldestNoteInArrayPoly
//    mostRecentVelocityInArray
//    placeNoteInFirstEmptyArrayPosition
//    placeNoteAndVelocityInFirstEmptyArrayPosition
//    eraseThisNoteInArray
//    testarrayContentsForAtLeastTwoNotes
//    findSingleNoteInArray
//    testArrayContentsForNoNotes
//    testArrayContentsForNoNotesPoly
//
//
// OSCILLATOR AND ARPEGGIATOR FUNCTIONS:
//
//    mainSampleEngine
//    mainPolyEngine
//    leadOneArpeggiator
//    mainLeadOneEngine
//    leadTwoArpeggiator
//    mainLeadTwoEngine
//    leadThreeArpeggiator
//    mainLeadThreeArpeggiator
//    leadFourArpeggiator
//    mainLeadFourArpeggiator
//
//
// MISC FUNCTIONS:
//
//    ledFlash
//
//
// SID FUNCTIONS:
//
//    writeFrequency
//    writePWM
//    writeWaveform
//    writeADSR
//    writeFilter
//    pulseCS
//
//
// MIDI CALLBACK FUNCTIONS:
//
//    OnNoteOn
//    OnNoteOff
//    OnControlChange
//    OnPitchChange
//
//
//
// The usbMIDI.read() in the main loop can trigger all of the MIDI callback
// functions, which update static variables in the oscillator and arpeggiator
// functions.
//
// All of the functions in the main loop have a 1 as their first argument,
// which allows them to update their portamento/arpeggiation/tremolo/sample.

