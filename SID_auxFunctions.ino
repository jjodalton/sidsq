//********************
// Auxiliary Functions
//********************
byte mostRecentNoteInArray (byte noteNumberArray[8], unsigned long noteDurationArray[8]) {
  byte i = 0;
  unsigned long mostRecentNoteDuration = 0;
  byte mostRecentNote = 0;

  for (i = 0; i < 8; i++) {
    if (noteDurationArray[i] > mostRecentNoteDuration) {
      mostRecentNoteDuration = noteDurationArray[i];
      mostRecentNote = noteNumberArray[i];
    }
  }
  return mostRecentNote;
}


void eraseOldestNoteInArray (byte (&noteNumberArray)[6], byte (&noteVelocityArray)[6], unsigned long (&noteDurationArray)[6]) {
  byte i = 0;
  unsigned long oldestDuration = 4294967295;
  byte oldestArrayPosition = 0;

  for (i = 0; i < 6; i++) {
    if (noteDurationArray[i] < oldestDuration) {
      oldestDuration = noteDurationArray[i];
      oldestArrayPosition = i;
    }
  }
  noteNumberArray[oldestArrayPosition] = 0;
  noteVelocityArray[oldestArrayPosition] = 0;
  noteDurationArray[oldestArrayPosition] = 0;
}


void eraseOldestNoteInArrayPoly (byte (&noteNumberArray)[3], byte (&noteVelocityArray)[3], unsigned long (&noteDurationArray)[3]) {
  byte i = 0;
  unsigned long oldestDuration = 4294967295;
  byte oldestArrayPosition = 0;

  for (i = 0; i < 3; i++) {
    if (noteDurationArray[i] < oldestDuration) {
      oldestDuration = noteDurationArray[i];
      oldestArrayPosition = i;
    }
  }
  noteNumberArray[oldestArrayPosition] = 0;
  noteVelocityArray[oldestArrayPosition] = 0;
  noteDurationArray[oldestArrayPosition] = 0;
}


byte mostRecentVelocityInArray (byte noteVelocityArray[8], unsigned long noteDurationArray[8]) {
  byte i = 0;
  unsigned long mostRecentNoteDuration = 0;
  byte mostRecentVelocity = 0;

  for (i = 0; i < 8; i++) {
    if (noteDurationArray[i] > mostRecentNoteDuration) {
      mostRecentNoteDuration = noteDurationArray[i];
      mostRecentVelocity = noteVelocityArray[i];
    }
  }
  return mostRecentVelocity;
}


void placeNoteInFirstEmptyArrayPosition(byte (&noteNumberArray)[8], unsigned long (&noteDurationArray)[8], byte outsideNoteNumber) {
  boolean noteAlreadyStored = LOW;

  for (int arrayCounter = 0; arrayCounter < 8 ; arrayCounter++) {
    if (noteNumberArray[arrayCounter] == outsideNoteNumber) {
      noteAlreadyStored = HIGH;
      break;
    }
  }
  if (noteAlreadyStored == LOW) {
    for (int arrayCounter = 0; arrayCounter < 8; arrayCounter++) {
      if (noteNumberArray[arrayCounter] == 0) {
        noteNumberArray[arrayCounter] = outsideNoteNumber;
        noteDurationArray[arrayCounter] = millis();
        break;
      }
    }
  }
}


void placeNoteAndVelocityInFirstEmptyArrayPosition( byte (&noteNumberArray)[8], unsigned long (&noteDurationArray)[8], byte outsideNoteNumber, byte (&noteVelocityArray)[8], byte outsideNoteVelocity) {
  boolean noteAlreadyStored = LOW;

  for (int arrayCounter = 0; arrayCounter < 8 ; arrayCounter++) {
    if (noteNumberArray[arrayCounter] == outsideNoteNumber) {
      noteAlreadyStored = HIGH;
      break;
    }
  }
  if (noteAlreadyStored == LOW) {
    for (int arrayCounter = 0; arrayCounter < 8; arrayCounter++) {
      if (noteNumberArray[arrayCounter] == 0) {
        noteNumberArray[arrayCounter] = outsideNoteNumber;
        noteDurationArray[arrayCounter] = millis();
        noteVelocityArray[arrayCounter] = outsideNoteVelocity;
        break;
      }
    }
  }
}


void eraseThisNoteInArray(byte (&noteNumberArray)[8], unsigned long (&noteDurationArray)[8], byte outsideNoteNumber) {
  for (int arrayCounter = 0; arrayCounter < 8; arrayCounter++) {
    if (noteNumberArray[arrayCounter] == outsideNoteNumber) {
      noteNumberArray[arrayCounter] = 0;
      noteDurationArray[arrayCounter] = 0;
    }
  }
}


boolean testArrayContentsForAtLeastTwoNotes (byte noteNumberArray[8]) {
  byte testArrayContents1 = 0;
  byte testArrayContents2 = 0;
  byte i = 0;
  byte j = 0;

  while (i < 8) {
    if (noteNumberArray[i] > 0) {
      testArrayContents1 = noteNumberArray[i];
      j = i + 1;
      while (j < 8) {
        if (noteNumberArray[j] > 0) {
          testArrayContents2 = noteNumberArray[j];
          break;
        }
        j++;
      }
      break;
    }
    i++;
  }
  if ((testArrayContents1 > 0) && (testArrayContents2 > 0)) {
    return HIGH;
  }
  else {
    return LOW;
  }
}


byte findSingleNoteInArray (byte noteNumberArray[8]) {
  byte arrayContents = 0;
  byte i = 0;

  while (i < 8) {
    if (noteNumberArray[i] > 0) {
      arrayContents = noteNumberArray[i];
      break;
    }
    i++;
  }
  return arrayContents;
}


boolean testArrayContentsForNoNotes (byte noteNumberArray[8]) {
  byte testArrayContents1 = 0;
  byte testArrayContents2 = 0;
  byte i = 0;
  byte j = 0;

  while (i < 8) {
    if (noteNumberArray[i] > 0) {
      testArrayContents1 = noteNumberArray[i];
      j = i + 1;
      while (j < 8) {
        if (noteNumberArray[j] > 0) {
          testArrayContents2 = noteNumberArray[j];
          break;
        }
        j++;
      }
      break;
    }
    i++;
  }
  if ((testArrayContents1 == 0) && (testArrayContents2 == 0)) {
    return HIGH;
  }
  else {
    return LOW;
  }
}


boolean testArrayContentsForNoNotesPoly (byte noteNumberArray[3]) {
  byte testArrayContents1 = 0;
  byte testArrayContents2 = 0;
  byte i = 0;
  byte j = 0;

  while (i < 3) {
    if (noteNumberArray[i] > 0) {
      testArrayContents1 = noteNumberArray[i];
      j = i + 1;
      while (j < 3) {
        if (noteNumberArray[j] > 0) {
          testArrayContents2 = noteNumberArray[j];
          break;
        }
        j++;
      }
      break;
    }
    i++;
  }
  if ((testArrayContents1 == 0) && (testArrayContents2 == 0)) {
    return HIGH;
  }
  else {
    return LOW;
  }
}
