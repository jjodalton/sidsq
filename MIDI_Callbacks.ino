void OnNoteOn (byte channel, byte note, byte velocity) {
  if (noVelocity == HIGH) {
    velocity = 127;
  }
  switch (channel) {
    case 1: //sample engine
      leadOneArpeggiator(2, note, velocity, 0, 0);
      mainLeadOneEngine(2, note, 0, LOW, 0, 0);
      break;

    case 2:
      leadTwoArpeggiator(2, note, velocity, 0, 0);
      mainLeadTwoEngine(2, note, 0, LOW, 0, 0);
      break;

    case 3:
      leadThreeArpeggiator(2, note, velocity, 0, 0);
      mainLeadThreeEngine(2, note, 0, LOW, 0, 0);
      break;

    case 4:
      leadFourArpeggiator(2, note, velocity, 0, 0);
      mainLeadFourEngine(2, note, 0, LOW, 0, 0);
      break;

    case 5:
      mainSampleEngine(2, note, velocity, 0);
      break;

    case 6:
      mainPolyEngine(2, note, velocity);
      break;
  }
  ledFlash(2);
}


void OnNoteOff(byte channel, byte note, byte velocity) {
  switch (channel) {
    case 1:
      leadOneArpeggiator(3, note, velocity, 0, 0);
      mainLeadOneEngine(3, note, 0, LOW, 0, 0);
      break;

    case 2:
      leadTwoArpeggiator(3, note, velocity, 0, 0);
      mainLeadTwoEngine(3, note, 0, LOW, 0, 0);
      break;

    case 3:
      leadThreeArpeggiator(3, note, velocity, 0, 0);
      mainLeadThreeEngine(3, note, 0, LOW, 0, 0);
      break;

    case 4:
      leadFourArpeggiator(2, note, velocity, 0, 0);
      mainLeadFourEngine(2, note, 0, LOW, 0, 0);
      break;

    case 5:
      mainSampleEngine(3, note, velocity, 0);
      break;

    case 6:
      mainPolyEngine(3, note, velocity);
      break;
  }
}


void OnControlChange(byte channel, byte control, byte value) {
  switch (control) {

    // Mod wheel, which controls different things for different engines.
    case 1:
      switch (channel) {
        case 1:
          mainLeadOneEngine(5, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(5, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(5, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(5, value, 0, LOW, 0, 0);
          break;

        case 5:
          mainSampleEngine(5, value, 0, 0);
          break;

        case 6:
          mainPolyEngine(5, value, 0);
          break;
      }
      break;

    case 2: // Global pitch bend range change.
      bendRange = map(value, 0, 127, 1, 12);
      break;

    case 3:
      break;

    case 4: // Arpeggiator On/Off
      switch (channel) {
        case 1:
          leadOneArpeggiator(4, value, 0, 0, 0);
          break;

        case 2:
          leadTwoArpeggiator(4, value, 0, 0, 0);
          break;

        case 3:
          leadThreeArpeggiator(4, value, 0, 0, 0);
          break;

        case 4:
          leadFourArpeggiator(4, value, 0, 0, 0);
          break;
      }
      break;

    case 5: // Arpeggiator Speed
      switch (channel) {
        case 1:
          leadOneArpeggiator(5, value, 0, 0, 0);
          break;

        case 2:
          leadTwoArpeggiator(5, value, 0, 0, 0);
          break;

        case 3:
          leadThreeArpeggiator(5, value, 0, 0, 0);
          break;

        case 4:
          leadFourArpeggiator(5, value, 0, 0, 0);
          break;

        case 5:
          break;

        case 6:
          break;
      }
      break;

    case 6: // Basic Waveform Selection
      switch (channel) {
        case 1:
          mainLeadOneEngine(9, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(9, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(9, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(9, value, 0, LOW, 0, 0);
          break;

        case 6:
          mainPolyEngine(9, value, 0);
          break;
      }
      break;

    case 7: // Waveform Modifier
      switch (channel) {
        case 1:
          mainLeadOneEngine(10, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(10, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(10, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(10, value, 0, LOW, 0, 0);
          break;

        case 6:
          mainPolyEngine(10, value, 0);
          break;
      }
      break;

    case 8: // Attack Amount
      switch (channel) {
        case 1:
          mainLeadOneEngine(11, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(11, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(11, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(11, value, 0, LOW, 0, 0);
          break;

        case 6:
          mainPolyEngine(11, value, 0);
          break;
      }
      break;

    case 9: // Decay Amount
      switch (channel) {
        case 1:
          mainLeadOneEngine(12, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(12, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(12, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(12, value, 0, LOW, 0, 0);
          break;

        case 6:
          mainPolyEngine(12, value, 0);
          break;
      }
      break;

    case 10: // Sustain Amount
      switch (channel) {
        case 1:
          mainLeadOneEngine(13, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(13, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(13, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(13, value, 0, LOW, 0, 0);
          break;

        case 6:
          mainPolyEngine(13, value, 0);
          break;
      }
      break;

    case 11: // Release Amount
      switch (channel) {
        case 1:
          mainLeadOneEngine(14, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(14, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(14, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(14, value, 0, LOW, 0, 0);
          break;

        case 6:
          mainPolyEngine(14, value, 0);
          break;
      }
      break;

    case 12: // PWM Amount
      switch (channel) {
        case 1:
          mainLeadOneEngine(15, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(15, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(15, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(15, value, 0, LOW, 0, 0);
          break;

        case 6:
          mainPolyEngine(15, value, 0);
          break;
      }
      break;

    case 13: // Filter Inclusion
      switch (channel) {
        case 1:
          if (value > 0) {
            writeFilter(2, 1, 0);
          }
          else {
            writeFilter(2, 4, 0);
          }
          break;

        case 2:
          if (value > 0) {
            writeFilter(2, 2, 0);
          }
          else {
            writeFilter(2, 5, 0);
          }
          break;

        case 3:
          if (value > 0) {
            writeFilter(2, 3, 0);
          }
          else {
            writeFilter(2, 6, 0);
          }
          break;

        case 5:
          if (value > 0) {
            writeFilter(2, 1, 0);
            writeFilter(2, 2, 0);
            writeFilter(2, 3, 0);
          }
          else {
            writeFilter(2, 4, 0);
            writeFilter(2, 5, 0);
            writeFilter(2, 6, 0);
          }
          break;
      }
      break;

    case 14: // Set Cutoff Frequency
      writeFilter(3, map(value, 0, 127, 0, 2047), 0);
      break;

    case 15: // Set Resonance
      writeFilter(4, map(value, 0, 127, 0, 15), 0);
      break;

    case 16: // Set Volume
      writeFilter(5, map(value, 0, 127, 0, 15), 0);
      break;

    case 17: // Set Filter Type
      writeFilter(6, map(value, 0, 127, 1, 3), 0);
      break;

    case 18: // Mute Three
      if (value > 0) {
        writeFilter(7, 1, 0);
      }
      else {
        writeFilter(7, 0, 0);
      }
      break;

    /*
      case 19: // Turn on sample mode
      if (value > 0){

        // Include ext audio DC offset for sample playback
        writeFilter(8,1,0);
      }
      else {
        writeFilter(8,0,0);
      }
      break;
    */

    case 20: // Porta Speed
      switch (channel) {
        case 1:
          mainLeadOneEngine(16, value, 0, LOW, 0, 0);
          break;

        case 2:
          mainLeadTwoEngine(16, value, 0, LOW, 0, 0);
          break;

        case 3:
          mainLeadThreeEngine(16, value, 0, LOW, 0, 0);
          break;

        case 4:
          mainLeadFourEngine(16, value, 0, LOW, 0, 0);
          break;
      }
      break;
  }
  ledFlash(2);
}


void OnPitchChange(byte channel, int pitch) {
  float bendRangeInCoefficient;
  float bendFactor;

  bendRangeInCoefficient = (pow(2, ((float)bendRange / 12)));
  bendFactor = pow(bendRangeInCoefficient, (((float)pitch / 8192.0) - 1.0));

  switch (channel) {
    case 1:
      mainLeadOneEngine(4, 0, bendFactor, LOW, 0, 0);
      break;

    case 2:
      mainLeadTwoEngine(4, 0, bendFactor, LOW, 0, 0);
      break;

    case 3:
      mainLeadThreeEngine(4, 0, bendFactor, LOW, 0, 0);
      break;

    case 4:
      mainLeadFourEngine(4, 0, bendFactor, LOW, 0, 0);
      break;

    case 5:
      mainSampleEngine(4, 0, 0, pitch);
      break;

    case 6:
      break;

    default:
      break;
  }
  ledFlash(2);
}
