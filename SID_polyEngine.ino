//******************
// Polyphonic Engine
//******************
void mainPolyEngine(byte dataType, byte noteNumber, byte noteVelocity) {
  static byte noteNumberArray[3];
  static byte noteVelocityArray[3];
  static unsigned long noteDurationsArray[3];
  static boolean insideRun = LOW;
  static boolean decayOn = LOW;
  static boolean arrayFull = LOW;
  static unsigned int decayInterval;
  static unsigned long previousDecayMicros;
  unsigned long currentMicros;
  static byte currentVelocityLevel1;
  static byte currentVelocityLevel2;
  static byte currentVelocityLevel3;
  static boolean tremOn = LOW;
  static int tremOffset = 0;
  static unsigned int tremInterval = 1000;
  static unsigned long previousTremMicros;
  unsigned long currentMillis;
  static float tremRateDivisor = 42.0; // Smaller is faster.
  static float tremAmount;
  static float tremAmount2;
  static int tremMaxDepth = 127;
  static byte insideCC1 = 0;
  static byte polyOneChip = 0;
  static byte leadOneChannel = 1;
  static byte leadTwoChannel = 2;
  static byte leadThreeChannel = 3;

  static byte attackAmount = 0;
  static byte decayAmount = 0;
  static byte sustainAmount = 15;
  static byte releaseAmount = 0;
  static unsigned int adsr = 0xF0;

  static byte basicWaveform = B1000000;
  static byte basicWaveformNumber = 0;
  static byte waveformModifier = B0;
  static byte waveformModifierNumber = 0;
  static byte waveformCombo = B1000000;
  static unsigned int pwmAmount = 2048;

  static byte tremVolume = 15;

  switch (dataType) {
    case 1: // Main Loop Run
      if ((insideRun == HIGH) && (insideCC1 > 0)) {
        currentMicros = micros();
        if ((currentMicros - previousTremMicros) >= tremInterval) {
          currentMillis = millis();
          tremAmount = sin(((float)currentMillis * 1.0 / (float)tremRateDivisor));
          tremAmount2 = tremAmount * (float)tremMaxDepth;
          tremOffset = tremAmount2 * ((float)insideCC1 / 127.0);
          if (tremOffset < 0) {  // Rectifies negative values.
            tremVolume = 15 - map(tremOffset, -127, 0, 15, 0);
          }
          else {
            tremVolume = 15 - map(tremOffset, 0, 127, 0, 15);
          }
          writeFilter(5, tremVolume, 0);
          previousTremMicros = micros();
        }
      }
      break;

    case 2: // Note On
      insideRun = HIGH;
      arrayFull = HIGH;
      for (byte i = 0; i < 3; i++) {
        if (noteNumberArray[i] == 0) {
          arrayFull = LOW;
        }
      }
      if (arrayFull == HIGH) {
        eraseOldestNoteInArrayPoly(noteNumberArray, noteVelocityArray, noteDurationsArray);
      }
      for (byte i = 0; i < 3; i++) {
        if ((noteNumberArray[i] == 0) && (noteNumber > 0)) {
          noteNumberArray[i] = noteNumber;
          noteVelocityArray[i] = noteVelocity;
          noteDurationsArray[i] = millis();
          float pitchInHertz;
          pitchInHertz = 440.00 * pow(2, ((noteNumber - 69) / 12.00));
          switch (i) {
            case 0:
              writeFrequency(pitchInHertz, leadOneChannel, polyOneChip);
              writeWaveform((waveformCombo | 0x1), leadOneChannel, polyOneChip);
              currentVelocityLevel1 = noteVelocity;
              break;

            case 1:
              writeFrequency(pitchInHertz, leadTwoChannel, polyOneChip);
              writeWaveform((waveformCombo | 0x1), leadTwoChannel, polyOneChip);
              currentVelocityLevel2 = noteVelocity;
              break;

            case 2:
              writeFrequency(pitchInHertz, leadThreeChannel, polyOneChip);
              writeWaveform((waveformCombo | 0x1), leadThreeChannel, polyOneChip);
              currentVelocityLevel3 = noteVelocity;
              break;
          }
          break;
        }
      }
      break;

    case 3: // Note Off
      for (byte i = 0; i < 3; i++) {
        if ((noteNumberArray[i] == noteNumber) && (noteNumber > 0)) {
          noteNumberArray[i] = 0;
          noteVelocityArray[i] = 0;
          noteDurationsArray[i] = 0;
          switch (i) {
            case 0:
              writeWaveform((waveformCombo & B11111110), leadOneChannel, polyOneChip);
              break;

            case 1:
              writeWaveform((waveformCombo & B11111110), leadTwoChannel, polyOneChip);
              break;

            case 2:
              writeWaveform((waveformCombo & B11111110), leadThreeChannel, polyOneChip);
              break;
          }
          if (testArrayContentsForNoNotesPoly(noteNumberArray) == HIGH) {
            writeWaveform((waveformCombo & B11111110), leadOneChannel, polyOneChip);
            writeWaveform((waveformCombo & B11111110), leadTwoChannel, polyOneChip);
            writeWaveform((waveformCombo & B11111110), leadThreeChannel, polyOneChip);
            insideRun = LOW;
          }
          break;
        }
      }
      break;

    case 4: // Pitch Bend (Unused)
      break;

    case 5: // CC 1 Change
      insideCC1 = noteNumber;
      if (insideCC1 == 0) {
        tremVolume = 15;
        writeFilter(5, tremVolume, 0);
      }
      break;

    case 6:
      break;

    case 9: // Basic Waveform Selection
      basicWaveformNumber = map(noteNumber, 0, 127, 0, 7);
      switch (basicWaveformNumber) {
        case 0: // Pulse Only
          basicWaveform = B1000000;
          break;

        case 1: // Saw Only
          basicWaveform = B100000;
          break;

        case 2: // Triangle Only
          basicWaveform = B10000;
          break;

        case 3: // Pulse/Saw
          basicWaveform = B1100000;
          break;

        case 4: // Pulse/Triangle
          basicWaveform = B1010000;
          break;

        case 5: // Saw/Triangle
          basicWaveform = B110000;
          break;

        case 6: // Pulse/Saw/Triangle
          basicWaveform = B1110000;
          break;

        case 7: // Noise
          basicWaveform = B10000000;
          break;
      }
      waveformCombo = basicWaveform | waveformModifier;
      if (insideRun == HIGH) {
        writeWaveform((waveformCombo | 0x1), leadOneChannel, polyOneChip);
        writeWaveform((waveformCombo | 0x1), leadTwoChannel, polyOneChip);
        writeWaveform((waveformCombo | 0x1), leadThreeChannel, polyOneChip);
      }
      else {
        writeWaveform((waveformCombo & B11111110), leadOneChannel, polyOneChip);
        writeWaveform((waveformCombo & B11111110), leadTwoChannel, polyOneChip);
        writeWaveform((waveformCombo & B11111110), leadThreeChannel, polyOneChip);
      }
      break;

    case 10: // Waveform Modifier Selection
      waveformModifierNumber = map(noteNumber, 0, 127, 0, 3);
      switch (waveformModifierNumber) {
        case 0: // Normal
          waveformModifier = B0;
          break;

        case 1: // Ring Mod
          waveformModifier = B100;
          break;

        case 2: // Sync
          waveformModifier = B10;
          break;

        case 3: // Ring Mod and Sync
          waveformModifier = B110;
          break;
      }
      waveformCombo = basicWaveform | waveformModifier;
      if (insideRun == HIGH) {
        writeWaveform((waveformCombo | 0x1), leadOneChannel, polyOneChip);
        writeWaveform((waveformCombo | 0x1), leadTwoChannel, polyOneChip);
        writeWaveform((waveformCombo | 0x1), leadThreeChannel, polyOneChip);
      }
      else {
        writeWaveform((waveformCombo & B11111110), leadOneChannel, polyOneChip);
        writeWaveform((waveformCombo & B11111110), leadTwoChannel, polyOneChip);
        writeWaveform((waveformCombo & B11111110), leadThreeChannel, polyOneChip);
      }
      break;

    case 11: // Attack Amount
      attackAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadOneChannel, polyOneChip);
      writeADSR(adsr, leadTwoChannel, polyOneChip);
      writeADSR(adsr, leadThreeChannel, polyOneChip);
      break;

    case 12: // Decay Amount
      decayAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadOneChannel, polyOneChip);
      writeADSR(adsr, leadTwoChannel, polyOneChip);
      writeADSR(adsr, leadThreeChannel, polyOneChip);
      break;

    case 13: // Sustain Amount
      sustainAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadOneChannel, polyOneChip);
      writeADSR(adsr, leadTwoChannel, polyOneChip);
      writeADSR(adsr, leadThreeChannel, polyOneChip);
      break;

    case 14: // Release Amount
      releaseAmount = map(noteNumber, 0, 127, 0, 15);
      adsr = (attackAmount << 12) | (decayAmount << 8) | (sustainAmount << 4) | releaseAmount;
      writeADSR(adsr, leadOneChannel, polyOneChip);
      writeADSR(adsr, leadTwoChannel, polyOneChip);
      writeADSR(adsr, leadThreeChannel, polyOneChip);
      break;

    case 15: // PWM Amount
      pwmAmount = map(noteNumber, 0, 127, 2048, 0);
      writePWM(pwmAmount, leadOneChannel, polyOneChip);
      writePWM(pwmAmount, leadTwoChannel, polyOneChip);
      writePWM(pwmAmount, leadThreeChannel, polyOneChip);
      break;
  }
}
