//**************
// Sample Engine
//**************
void mainSampleEngine(byte dataType, byte noteNumber, byte noteVelocity, long longData) {

  static boolean insideRun = LOW;
  static unsigned long sampleCounter = 0;
  static byte sampleCounterIncrement = 1;
  static unsigned long maxSampleCounter = 400;
  byte current8bitSampleLevel;
  byte current4bitSampleLevel;
  static byte sampleNumber;
  static byte insideNoteNumber;
  unsigned long currentMicros;
  static unsigned long previousMicros;
  unsigned long elapsedTimeInMicros;
  static unsigned long sampleRateMicros = 250;
  static byte sampleIncrementRate = 1;

  switch (dataType) {
    case 1: // Run in main loop
      if (insideRun == HIGH) {
        if (sampleCounter >= maxSampleCounter) {
          sampleCounter = 0;
          insideRun = LOW;
        }
      }
      if (insideRun == HIGH) {
        currentMicros = micros();
        elapsedTimeInMicros = currentMicros - previousMicros;
        if (elapsedTimeInMicros >= sampleRateMicros) {
          current8bitSampleLevel = pgm_read_byte_near(sampleData + (sampleStarts[sampleNumber]) + sampleCounter);
          current4bitSampleLevel = (current8bitSampleLevel >> 4) & B00001111;
          writeFilter(5, current4bitSampleLevel, 0);
          sampleCounter = sampleCounter + sampleIncrementRate;
          previousMicros = micros();
        }
      }
      break;

    case 2: // Note On
      if ((insideRun == LOW) || (insideRun == HIGH)) {
        insideNoteNumber = noteNumber;
        if ((noteNumber >= 48) && (noteNumber <= 66)) {
          sampleNumber = noteNumber - 48;
          sampleCounter = 0;
          maxSampleCounter = sampleStarts[sampleNumber + 1] - sampleStarts[sampleNumber];
          insideRun = HIGH;
        }
      }
      break;

    case 3: // Note Off
      if (insideNoteNumber == noteNumber) {
        insideRun = LOW;
      }
      break;

    case 4: // Pitch Bend
      if (longData == 8192) {
        sampleRateMicros = 250;
      }
      if (longData <= 8191) {
        sampleRateMicros = map(longData, 0, 8191, 150, 249);
      }
      if (longData >= 8193) {
        sampleRateMicros = map(longData, 8193, 16838, 251, 75);
      }
      break;

    case 5: // CC1 Change
      if (noteNumber > 0) {
        sampleRateMicros = 251 * pow(200, ((float)noteNumber / 127));
      }
      else {
        sampleRateMicros = 250;
      }
      break;
  }
}
