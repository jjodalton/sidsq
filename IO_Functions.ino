

void setupIO() {
  // Setup Indicator Pins
  pinMode(LED, OUTPUT);

  //Setup Chip Clock Pins
  pinMode(chipClock, OUTPUT);

  //Setup Chip Control Pins
  pinMode(chipSelect0, OUTPUT);
  pinMode(chipSelect1, OUTPUT);
  pinMode(RW, OUTPUT);
  pinMode(nReset, OUTPUT);

  //Setup Address Bus Pins
  pinMode(ad0, OUTPUT);
  pinMode(ad1, OUTPUT);
  pinMode(ad2, OUTPUT);
  pinMode(ad3, OUTPUT);
  pinMode(ad4, OUTPUT);

  //Setup Data Bus Pins
  pinMode(da0, OUTPUT);
  pinMode(da1, OUTPUT);
  pinMode(da2, OUTPUT);
  pinMode(da3, OUTPUT);
  pinMode(da4, OUTPUT);
  pinMode(da5, OUTPUT);
  pinMode(da6, OUTPUT);
  pinMode(da7, OUTPUT);
}

void testIO() {
  // Cycle through adress bus bits individually
  digitalWrite(ad0, HIGH);
  delay(100);
  digitalWrite(ad0, LOW);
  digitalWrite(ad1, HIGH);
  delay(100);
  digitalWrite(ad1, LOW);
  digitalWrite(ad2, HIGH);
  delay(100);
  digitalWrite(ad2, LOW);
  digitalWrite(ad3, HIGH);
  delay(100);
  digitalWrite(ad3, LOW);
  digitalWrite(ad4, HIGH);
  delay(100);
  digitalWrite(ad4, LOW);
  delay(100);

  // Group write of address bus
  GPIOC_PDOR |= 0x1F; // Set
  delay(300);
  GPIOC_PDOR &= ~0x1F; // Clear


  // Cycle through data bus bits
  digitalWrite(da0, HIGH);
  delay(100);
  digitalWrite(da0, LOW);
  digitalWrite(da1, HIGH);
  delay(100);
  digitalWrite(da1, LOW);
  digitalWrite(da2, HIGH);
  delay(100);
  digitalWrite(da2, LOW);
  digitalWrite(da3, HIGH);
  delay(100);
  digitalWrite(da3, LOW);
  digitalWrite(da4, HIGH);
  delay(100);
  digitalWrite(da4, LOW);
  digitalWrite(da5, HIGH);
  delay(100);
  digitalWrite(da5, LOW);
  digitalWrite(da6, HIGH);
  delay(100);
  digitalWrite(da6, LOW);
  digitalWrite(da7, HIGH);
  delay(100);
  digitalWrite(da7, LOW);
  delay(100);

  // Group write of data bus
  GPIOD_PDOR |= ( 0xFF ); // Set
  delay(300);
  GPIOD_PDOR &= ~( 0xFF ); // Clear
}

void writeFrequency(float pitchInHertz, byte channel, byte chip) {
  // 0-65535 in Hz
  byte addressLowByte[3] = {0x0, 0x7, 0xE};
  byte addressHighByte[3] = {0x1, 0x8, 0xF};
  byte lowByteOfFrequency;
  byte highByteOfFrequency;
  unsigned int frequencyValue;

  channel = channel - 1;

  frequencyValue = pitchInHertz / 0.0596;

  lowByteOfFrequency = frequencyValue & 0xFF;
  GPIOC_PDOR = addressLowByte[channel];
  GPIOD_PDOR = lowByteOfFrequency;
  pulseCS(chip);

  highByteOfFrequency = frequencyValue >> 0x8;
  GPIOC_PDOR = addressHighByte[channel];
  GPIOD_PDOR = highByteOfFrequency;
  pulseCS(chip);
}


void writePWM(unsigned int width, byte channel, byte chip) {
  // 0-4095
  byte addressLowByte[3] = {0x2, 0x9, 0x10};
  byte addressHighByte[3] = {0x3, 0xA, 0x11};
  byte lowByteOfWidth;
  byte highByteOfWidth;

  channel = channel - 1;

  lowByteOfWidth = width & 0xFF;
  GPIOC_PDOR = addressLowByte[channel];
  GPIOD_PDOR = lowByteOfWidth;
  pulseCS(chip);

  highByteOfWidth = width >> 0x8;
  GPIOC_PDOR = addressHighByte[channel];
  GPIOD_PDOR = highByteOfWidth;
  pulseCS(chip);
}


void writeWaveform(byte waveform, byte channel, byte chip) {
  // (MSB -> LSB) Noise, Pulse, Saw, Triangle, ZERO, Ring Mod, Sync, Gate
  byte controlAddress[3] = {0x4, 0xB, 0x12};

  channel = channel - 1;

  GPIOC_PDOR = controlAddress[channel];
  GPIOD_PDOR = waveform & B11110111;
  pulseCS(chip);
}


void writeADSR(unsigned int adsr, byte channel, byte chip) {
  // A Nibble For Each Component (A is MSB, R is LSB)
  byte addressLowByte[3] = {0x6, 0xD, 0x14};
  byte addressHighByte[3] = {0x5, 0xC, 0x13};
  byte lowByteOfADSR;
  byte highByteOfADSR;

  channel = channel - 1;

  lowByteOfADSR = adsr & 0xFF;
  GPIOC_PDOR = addressLowByte[channel];
  GPIOD_PDOR = lowByteOfADSR;
  pulseCS(chip);

  highByteOfADSR = adsr >> 0x8;
  GPIOC_PDOR = addressHighByte[channel];
  GPIOD_PDOR = highByteOfADSR;
  pulseCS(chip);
}


void writeFilter(byte dataType, unsigned int uIntData, byte chip) {
  static byte lowByteOfCutoff = B00000000;
  static byte highByteOfCutoff = B00000000;
  static boolean filterOnOne = 0;
  static boolean filterOnTwo = 0;
  static boolean filterOnThree = 0;
  static byte resonanceNibble = 0;
  static byte volumeNibble = 15;
  static byte filterType = B00010000;
  static byte threeMute = B00000000;
  static byte filterExternal = 0;

  switch (dataType) {
    case 0: // Initialization
      GPIOC_PDOR = 0x15;
      GPIOD_PDOR = B00000000;
      pulseCS(chip);

      GPIOC_PDOR = 0x16;
      GPIOD_PDOR = B01100100;
      pulseCS(chip);

      GPIOC_PDOR = 0x17;
      GPIOD_PDOR = B00000000;
      pulseCS(chip);

      GPIOC_PDOR = 0x18;
      GPIOD_PDOR = B00011111;
      pulseCS(chip);
      break;

    case 1: // Running
      break;

    case 2: // Turn Filter On For A Channel (Make second argument 1-3 to turn on, 4-6 to turn off.)
      switch (uIntData) {
        case 1:
          filterOnOne = 1;
          break;
        case 2:
          filterOnTwo = 1;
          break;
        case 3:
          filterOnThree = 1;
          break;
        case 4:
          filterOnOne = 0;
          break;
        case 5:
          filterOnTwo = 0;
          break;
        case 6:
          filterOnThree = 0;
          break;
      }
      GPIOC_PDOR = 0x17;
      GPIOD_PDOR = filterOnOne | (filterOnTwo << 1) | (filterOnThree << 2) | (filterExternal << 3) | (resonanceNibble << 4);
      pulseCS(chip);
      break;

    case 3: // Set Cutoff Frequency (0-2047)
      if (uIntData > 2047) {
        uIntData = 2047;
      }
      lowByteOfCutoff = uIntData & 0x7;
      GPIOC_PDOR = 0x15;
      GPIOD_PDOR = lowByteOfCutoff;
      pulseCS(chip);

      highByteOfCutoff = uIntData >> 0x03;
      GPIOC_PDOR = 0x16;
      GPIOD_PDOR = highByteOfCutoff;
      pulseCS(chip);
      break;

    case 4: // Set Resonance (0-15)
      if (uIntData > 15) {
        uIntData = 15;
      }
      resonanceNibble = uIntData & B00001111;

      GPIOC_PDOR = 0x17;
      GPIOD_PDOR = filterOnOne | (filterOnTwo << 1) | (filterOnThree << 2) | (filterExternal << 3) | (resonanceNibble << 4);
      pulseCS(chip);
      break;

    case 5: // Set Volume (0-15)
      if (uIntData > 15) {
        uIntData = 15;
      }
      volumeNibble = uIntData & B00001111;

      GPIOC_PDOR = 0x18;
      GPIOD_PDOR = volumeNibble | filterType | threeMute;
      pulseCS(chip);
      break;

    case 6: // Set Filter Type (1-3)
      if (uIntData == 1) { // LP
        filterType = B00010000;
      }
      if (uIntData == 2) { // BP
        filterType = B00100000;
      }
      if (uIntData == 3) { // HP
        filterType = B01000000;
      }

      GPIOC_PDOR = 0x18;
      GPIOD_PDOR = volumeNibble | filterType | threeMute;
      pulseCS(chip);
      break;

    case 7: // Mute Three
      if (uIntData == 1) {
        threeMute = B10000000;
      }
      else {
        threeMute = B00000000;
      }

      GPIOC_PDOR = 0x18;
      GPIOD_PDOR = volumeNibble | filterType | threeMute;
      pulseCS(chip);
      break;

    case 8: // Filter External Audio Pin
      if (uIntData == 1) {
        filterExternal = 1;
      }
      else {
        filterExternal = 0;
      }

      GPIOC_PDOR = 0x17;
      GPIOD_PDOR = filterOnOne | (filterOnTwo << 1) | (filterOnThree << 2) | (filterExternal << 3) | (resonanceNibble << 4);
      pulseCS(chip);
      break;
  }
}


void pulseCS(byte chipNumber) {
  if (chipNumber == 0) {
    digitalWrite(chipSelect0, LOW);
    delayMicroseconds(transferHoldTime);
    digitalWrite(chipSelect0, HIGH);
  }
  else if(chipNumber == 1) {
    digitalWrite(chipSelect1, LOW);
    delayMicroseconds(transferHoldTime);
    digitalWrite(chipSelect1, HIGH);
  }
  else {
    Serial.println("Chip Select Error");
  }
}
